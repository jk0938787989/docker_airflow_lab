import datetime
import os
import shutil
import sys

def createDagFile(dagFileName):

    boo = False
    try:
        f = open(dagFileName, "x")
        print('[System log] The Dag python create !! ')
        boo = True
    except:
        print('[System log] The Dag name has exist!')
        keepgo = input("Whether cover exist file or not (Y/N)?")
        if keepgo == 'Y':
            boo = True
        else:
            boo = False
    return boo

def writeDagFile(dagFileName, inputString):

    boo = False
    try:
        newdag = open(dagFileName, 'a')
        newdag.write(inputString)
        newdag.close()
        print("[System log] Sucessful Write Dagfile !!")
        boo = True
    except:
        print("[System log] Error Write Dagfile !!")
        boo = False

    return boo

def importPackage(dagFileName):

    boo = False

    packages_context = "import os\n"\
                        "import shutil\n"\
                        "import airflow\n"\
                        "from datetime import datetime, timedelta\n"\
                        "from airflow import DAG\n"\
                        "from airflow.operators.bash_operator import BashOperator\n"\
                        "from airflow.operators.python_operator import PythonOperator\n"

    boo = writeDagFile(dagFileName, packages_context)
    
    return boo


def initCreateArg(dagFileName):

    boo = False

    dag_onwer = input("Dag owner : ")
    while dag_onwer == "":
        print('[System log] Dag owner Error ...  ')
        dag_onwer = input("Press again dag owner : ")

    dag_email = input("Email : ")
    while '@' not in dag_email:
        print('[System log] Email format Error ...  ')
        dag_email = input("Press again Email : ")

    dag_email_on_failure = input("If task fail, send mail? (True/False, default is False) : ")
    if dag_email_on_failure == "" or dag_email_on_failure != 'True': dag_email_on_failure = 'False'

    dag_email_on_retry = input("If task retry, send mail? (True/False, default is False) : ")
    if dag_email_on_retry == "" or dag_email_on_retry != 'True': dag_email_on_retry = 'False'

    dag_depends_on_past = input("Depends on past ? (True/False, default is False) : ")
    if dag_depends_on_past == "" or dag_depends_on_past != 'True': dag_depends_on_past = 'False'

    while True:
        try:
            dag_retries = input("Retry times (default is 1) : ")
            if dag_retries == "":
                dag_retries = 1
            dag_retries = int(dag_retries)
            break
        except:
            print("[Format error] Please enter an integer... ")
            continue

    while True:
        try:
            dag_retry_delay = input("Retry delay time (default is 1) : ")
            if dag_retry_delay == "": 
                dag_retry_delay = 1
            dag_retry_delay = int(dag_retry_delay)
            break
        except:
            print("[Format error] Please enter an integer... ")
            continue

    while True:
        try:
            str_dag_start_date = input("Start date(yyyy-mm-dd) (default is two days ago): ")
            if str_dag_start_date == "" : 
                str_dag_start_date = (datetime.date.today() - datetime.timedelta(days=2)).strftime("%Y-%m-%d")
            dag_start_date = datetime.datetime.strptime(str_dag_start_date, "%Y-%m-%d")
            break
        except:
            print("[Format error] Please enter YYYY-MM-DD ... ")
            continue

    str_defaults_arg_context = "default_args = {{\n"\
                               "	'owner':'{:s}',\n" \
                               "	'email':['{:s}'],\n" \
                               "	'email_on_failure':{:s}, \n" \
                               "	'email_on_retry':{:s}, \n" \
                               "	'depends_on_past':{:s},\n"\
                               "	'start_date':datetime({:d},{:d},{:d}),\n"\
                               "	'retries':{:d},\n"\
                               "	'retry_delay':timedelta(minutes={:d}), \n"\
                               "}} \n"								

    defaults_arg = str_defaults_arg_context.format(dag_onwer, dag_email, 
            dag_email_on_failure,dag_email_on_retry, dag_depends_on_past, 
            dag_start_date.year, dag_start_date.month, dag_start_date.day,dag_retries, dag_retry_delay)

    print("[System log] Dag init complete !! ")

    boo = writeDagFile(dagFileName, defaults_arg)

    return boo

def settingSchedule():

    schedule_input = ""

    print("┌───────────── minute (0 - 59) \n" \
	  "│ ┌───────────── hour (0 - 23) \n" \
	  "│ │ ┌───────────── day of the month (1 - 31) \n" \
	  "│ │ │ ┌───────────── month (1 - 12) \n" \
	  "│ │ │ │ ┌───────────── day of the week (0 - 6) (Sunday to Saturday,7 is also Sunday on some systems \n" \
	  "│ │ │ │ │ \n" \
	  "│ │ │ │ │ \n" \
	  "* * * * * \n" \
	  "eg: 10 6 * * * >> at 6:10 a.m. every day \n " \
	  "if you want to cycle execute，use \"/\" to set. \n"\
	  "eg: 0 */2 * * * >> every two hours at the top of the hour \n"\
	  "eg: 0 23-7/2,8 * * * >> every two hours from 11p.m. to 7a.m., and at 8a.m. \n"
	)
    schedule_input = input("Please enter schedule format(Default > */1 * * * *) : ")

    if schedule_input == "":
        schedule_input = "*/1 * * * *"

    return schedule_input

def createMainDag(dagFileName):

    boo = False

    input_dag_id = input("Dag ID : ")
    while input_dag_id == "":
        print('[System log] Dag Id Error ...  ')
        input_dag_id = input("Press again Dag Id : ")

    dag_description = input("Dag description : ")
    dag_schedule_interval = settingSchedule()
    str_dag_context = "\n" \
		  "dag = DAG( \n" \
		  "    dag_id='{:s}',\n"\
		  "    description='{:s}',\n"\
		  "    default_args=default_args,\n"\
		  "    schedule_interval='{:s}'\n"\
		  ")\n"

    dag_parameter = str_dag_context.format(input_dag_id, dag_description, dag_schedule_interval)

    boo = writeDagFile(dagFileName, dag_parameter)

    return boo

def createDagTask(dagFileName):

    # 生成task個數
    boo = False

    while True:
        str_taskCount = input("Task Count :")
        if str_taskCount.isdigit() == False or isinstance(str_taskCount, float) or str_taskCount=='0':
            print("Please enter integer ...")
            continue
        else:
            break
    int_taskCount = int(str_taskCount)
    list_task_name = []
    list_task_id = []

    for i in range(0,int_taskCount):
        no_task = str(i+1)
        input_task_name = input("[Step " + no_task + "] Task Name :")
        while input_task_name in list_task_name:
            print("[System log]Task name has exist ...")
            input_task_name = input("Press again for Task Name :")			
		
        input_task_id = input("Task ID :")
        while input_task_id in list_task_id:
            print("[System log]Task ID has exist ...")
            input_task_id = input("Press again for Task ID :")

        list_task_name.append(input_task_name)
        list_task_id.append(input_task_id)
        
        input_python_path = input("Python file path (<path>/<pyname>.py) :")
        while os.path.isfile(input_python_path) == False:
            print("[System log] Python path error ...")
            input_python_path = input("ress again Python file path (<path>/<pyname>.py) :")
        execpyfile = input_python_path.split("/")[-1]
        target_dag_path = '/usr/local/airflow/run/{:s}'.format(execpyfile)
        str_task_context = "\n"\
			   "\n"\
			   "{:s} = BashOperator( \n"\
			   "	task_id='{:s}', \n"\
			   "	bash_command = 'python3 {:s}',\n"\
			   "	xcom_push = True,\n"\
			   "	dag = dag \n"\
			   ")\n"
        task_context = str_task_context.format(input_task_name,input_task_id,target_dag_path)

        writeDagFile(dagFileName, task_context)

        print("[Task "+no_task+"] setting complete ! ")

    task_taskSchedule = "\n"

    for task in list_task_name:
        task_taskSchedule = task_taskSchedule + task + " >> "

    task_taskSchedule = task_taskSchedule[:-3]
    boo = writeDagFile(dagFileName, task_taskSchedule)

    return boo

if __name__ == '__main__':

    boo = False

    dagname = input("Please enter dag file name : ")
    while dagname == "":
        print('[System log] Empty name for dag python file ...  ')
        dagname = input("Press again dagname : ")

    dagname_py = dagname + ".py"

    if createDagFile(dagname_py) == False:
        print("[System log] Main : Create Dag File error...")
        sys.exit()
    if importPackage(dagname_py) == False:
        print("[System log] Main : Import packages error...")
        sys.exit()
    if initCreateArg(dagname_py) == False:
        print("[System log] Main : Init defaults parameter error...")
        sys.exit()
    if createMainDag(dagname_py) == False:
        print("[System log] Main : Create dag info error...")
        sys.exit()
    if createDagTask(dagname_py) == False:
        print("[System log] Main : Create dag task error...")
        sys.exit()

	
    while True:
        try:
            target_dag_path = input("Enter dag path >> ")
            shutil.move(dagname_py,target_dag_path)
            print("[System log] Aiflow Dag file Complete !!! ")
            break
        except:
            print("Dag move to target path Fail ! ")
            continue


