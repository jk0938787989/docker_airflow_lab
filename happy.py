import os
import shutil
import airflow
from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.operators.python_operator import PythonOperator
default_args = {
	'owner':'KP',
	'email':['kasjl@ljalsdkj.com'],
	'email_on_failure':False, 
	'email_on_retry':False, 
	'depends_on_past':False,
	'start_date':datetime(2020,9,27),
	'retries':5,
	'retry_delay':timedelta(minutes=3), 
} 

dag = DAG( 
    dag_id='mydag',
    description='this is a test',
    default_args=default_args,
    schedule_interval='*/5 * * * *'
)


task1 = BashOperator( 
	task_id='test1', 
	bash_command = 'python3 /usr/local/airflow/run/printmytest.py',
	xcom_push = True,
	dag = dag 
)


task2 = BashOperator( 
	task_id='test2', 
	bash_command = 'python3 /usr/local/airflow/run/printmytest2.py',
	xcom_push = True,
	dag = dag 
)


task3 = BashOperator( 
	task_id='test3', 
	bash_command = 'python3 /usr/local/airflow/run/printmytest3.py',
	xcom_push = True,
	dag = dag 
)

task1 >> task2 >> task3 